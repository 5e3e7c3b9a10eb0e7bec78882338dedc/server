# Node.js Express API with TypeScript 3


#### error response
```
{
    "success": boolean,
    "status": number,
    "name": string,
    "message": string
}
```


## Register new user
`POST` /v1/user/register

#### request json

```
{
	"username": string,
	"fullname": string,
	"phone": string,
	"credentials": {
		"email": string,
		"password": string
	}
}
```

#### response json

```
{
    "success": boolean
}

```


## Login user
`POST` /v1/user/login
```
{
	"email": string,
	"password": string,
}
```


#### response json

```
{
    "hash": string
    "expires": number
}

```

## Confirm user email
`GET` /v1/user/confirm?user_id={OBJECT_ID}

#### response json

```
{
    "success": boolean
}

```

## Reset password request user
`POST` /v1/user/reset-password
```
{
	"email": string,
}
```
#### response json

```
{
  "success": string
}

```

## Update password
`POST` /v1/user/update-password
```
{
	"id": string,
  "password": string
}
```
#### response json

```
{
  "success": string
}

```