import { authenticate } from 'passport';
import { Router } from 'express';
import clusterValidation  from './cluster.validation';
import clusterController from './cluster.controller';


/**
 * @export
 * @class ClusterRouter
 */
class ClusterRouter {
  public router: Router;

  /**
   * Creates an instance of ClusterRouter.
   * @memberof ClusterRouter
   */
  constructor() {
    this.router = Router();
    this.routes();
  }

  /**
   * @memberof ClusterRouter
   */
  public routes(): void {
    
    this.router.get('/:userId',
      authenticate('bearer', { session: false }), 
      clusterValidation.getClusters.bind(clusterValidation),
      clusterController.get.bind(clusterController));

    this.router.post('/',
      authenticate('bearer', { session: false }), 
      clusterValidation.createCluster.bind(clusterValidation),
      clusterController.create.bind(clusterController));

      
    this.router.delete('/:clusterId',
      authenticate('bearer', { session: false }), 
      clusterValidation.removeCluster.bind(clusterValidation),
      clusterController.remove.bind(clusterController));
  }
}

export default new ClusterRouter();
