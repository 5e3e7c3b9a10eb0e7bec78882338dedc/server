import { IClusterModel } from './cluster.model';

/**
 * @export
 * @interface IClusterService
 */
export interface IClusterService {

  /**
   * @async
   * @param {string} userId
   * @returns {Promise<IClusterModel[]>}
   * @memberof IClusterService
   */
  getClusters(userId: string): Promise<IClusterModel[]>;

  /**
   * @async
   * @param {IClusterModel} cluster
   * @returns {Promise<IClusterModel>}
   * @memberof IClusterService
   */
  createCluster(cluster: IClusterModel): Promise<IClusterModel>;

  /**
   * @async
   * @param {string} clusterId
   * @returns {Promise<boolean[]>}
   * @memberof IClusterService
   */
  removeCluster(clusterId: string): Promise<boolean>;
}
