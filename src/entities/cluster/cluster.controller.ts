import { HttpError } from '../../config/error/index';
import { NextFunction, Request, Response } from 'express';
import clusterService from './cluster.service';
import { IClusterModel } from './cluster.model';
import { IClusterService } from './cluster.interface';

class ClusterController {

  /**
   * @private
   * @type {IClusterService}
   * @memberof ClusterController
   */
  private clusterService: IClusterService;

  /**
   * Creates an instance of ClusterController.
   * @memberof ClusterController
   */
  constructor(
    clusterService: IClusterService,
  ) {
    this.clusterService = clusterService;
  }

  /**
   * @async
   * @param {Request} req
   * @param {Response} res
   * @returns {Promise < Response >}
   * @memberof ClusterController
   */
  public async get(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const clusters: IClusterModel[] = await this.clusterService.getClusters(req.params.userId);

      return res.status(200).json(clusters);
    } catch (error) {
      next(new HttpError(error.status, error.message));
    }
  }

  /**
   * @async
   * @param {Request} req
   * @param {Response} res
   * @returns {Promise < Response >}
   * @memberof ClusterController
   */
  public async create(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const cluster: IClusterModel = await clusterService.createCluster(req.body);

      return res.status(200).json({ cluster, success: true });
    } catch (error) {
      next(new HttpError(error.status, error.message));
    }
  }

  /**
   * @async
   * @param {Request} req
   * @param {Response} res
   * @returns {Promise < Response >}
   * @memberof ClusterController
   */
  public async remove(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const success: boolean = await clusterService.removeCluster(req.params.clusterId);

      return res.status(200).json({ success });
    } catch (error) {
      next(new HttpError(error.status, error.message));
    }
  }
}

export default new ClusterController(clusterService);
