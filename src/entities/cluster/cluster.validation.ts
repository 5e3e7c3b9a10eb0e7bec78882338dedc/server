import * as Joi from 'joi';
import { Types } from 'mongoose';
import { NextFunction, Request, Response } from 'express';
import { errors } from '../../config/error/errorsTypes';
import HttpError from '../../config/error';
import { IClusterModel } from './cluster.model';

/**
 * @export
 * @class ClusterValidation
 */
class ClusterValidation {
  customJoi: any;

  /**
   * @static
   * @type {string}
   * @memberof JoiSchema
   */
  readonly messageObjectId: string =
    'Argument passed in must be a single String of 12 bytes or a string of 24 hex characters';

  /**
   * Creates an instance of Schema.
   * @memberof JoiSchema
   */
  constructor() {
    this.customJoi = Joi.extend({
      name: 'objectId',
      language: {
        base: this.messageObjectId
      },
      pre(value: any, state: Joi.State, options: Joi.ValidationOptions): any {
        if (!Types.ObjectId.isValid(value)) {
          return this.createError(
            'objectId.base', {
              value
            },
            state,
            options
          );
        }

        return value; // Keep the value as it was
      }
    });
  }

  /**
   * @param {Request} req
   * @param {Response} res
   * @param {Function} next
   * @returns void
   * @memberof getClusters
   */
  public getClusters(req: Request, res: Response, next: NextFunction): void {
    try {
      const body: { userId: string } = req.params;

      const schema: Joi.Schema = Joi.object().keys({
        userId: this.customJoi.objectId().required(),
      });
      const validate: Joi.ValidationResult<{ userId: string }> = Joi.validate(body, schema);

      if (validate.error) throw errors.getCustomError(400, validate.error.message);
      
      next();
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @param {Request} req
   * @param {Response} res
   * @param {Function} next
   * @returns void
   * @memberof getClusters
   */
  public createCluster(req: Request, res: Response, next: NextFunction): void {
    try {
      const schema: Joi.Schema = Joi.object().keys({
        name: Joi.string().max(50).required(),
        location: Joi.string().max(2).min(2).required(),  
        owner: { id: this.customJoi.objectId().required() } 
      });
      const validate: Joi.ValidationResult<IClusterModel> = Joi.validate(req.body, schema);

      if (validate.error) throw errors.getCustomError(400, validate.error.message);
      
      next();
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @public
   * @param {Request} req
   * @param {Response} res
   * @param {Function} next
   * @returns void
   * @memberof getClusters
   */
  public removeCluster(req: Request, res: Response, next: NextFunction): void {
    try {
      const body: { clusterId: string } = req.params;

      const schema: Joi.Schema = Joi.object().keys({
        clusterId: this.customJoi.objectId().required(),
      });
      const validate: Joi.ValidationResult<{ clusterId: string }> = Joi.validate(body, schema);

      if (validate.error) throw errors.getCustomError(400, validate.error.message);
      
      next();
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }
}

export default new ClusterValidation();
