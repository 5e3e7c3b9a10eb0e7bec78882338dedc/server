
import { HttpError } from '../../config/error/index';
import { IAWSService } from '../../services/aws/aws.interface';
import { ManagedUpload } from 'aws-sdk/lib/s3/managed_upload';
import AWSService from '../../services/aws/aws.service';
import ClusterModel,  { IClusterModel } from './cluster.model';
import { IClusterService } from './cluster.interface';
import userModel, { IUserModel } from '../user/user.model';
import { errors } from '../../config/error/errorsTypes';
import contentModel, { IContentModel } from '../content/content.model';

/**
 * @export
 * @class ContentService
 * @implements {IUserModelService}
 */
class ClusterService implements IClusterService {

  /**
   * @private
   * @type IAWSService
   * @memberof ClusterService
   */
  private awsService: IAWSService;

  /**
   * Creates an instance of ContentService.
   * @constructor
   * @memberof ClusterService
   */
  constructor(
    awsService: IAWSService
  ) {
    this.awsService = awsService;
  }

  /**
   * @async
   * @param {string} userId
   * @returns {<Promise <IClusterModel[]>}
   * @memberof ClusterService 
   */
  public async getClusters(userId: string): Promise<IClusterModel[]> {
    try {
      const condition: any = {
        $or: [
          { 'owner.id': userId },
          { allowedUsers: { $elemMatch: { id: userId } } }
        ]
      };

      const clusters: IClusterModel[] = await ClusterModel.find(condition); 

      return clusters;
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @async
   * @param {IClusterModel} cluster
   * @returns {<Promise <IClusterModel>}
   * @memberof ClusterService 
   */
  public async createCluster(cluster: IClusterModel): Promise<IClusterModel> {
    try {
      const user: IUserModel = await userModel.findById(cluster.owner.id);
      if (!user) throw errors.notFound;

      // TODO: test plan
      cluster.maxSize = 2147483648;
      cluster.convertedMaxSize = '2 GB';
      // end of TODO
      cluster.hashedName = await this.awsService.createCluster(cluster);
      cluster.owner = {
        id: user.id,
        role: 'owner',
        name: user.fullname
      };
      const newCluster: IClusterModel = new ClusterModel(cluster);
      
      return await newCluster.save();
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * Remove all files in AWS FOLDER before removing it. 
   * Solution becaus AWS SDC do not can delete AWS FOLDER with content
   * @async
   * @param {string} clusterId
   * @returns {Promise<boolean[]>}
   * @memberof ClusterService
   */
  public async removeCluster(clusterId: string): Promise<boolean> {
    try {
      const cluster: IClusterModel = await ClusterModel.findById(clusterId);
      if (!cluster) throw errors.notFound;

      const content: IContentModel[] = await contentModel.find({ $and: [{ type: { $ne: 'folder' } }, { clusterId: cluster.id }] });
      
      await contentModel.deleteMany({ clusterId: cluster.id });
      await cluster.remove();
      await this.awsService.deleteObjects(cluster, content);
      await this.awsService.deleteObjects(cluster);

      return true;
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }
}

export default new ClusterService(AWSService);
