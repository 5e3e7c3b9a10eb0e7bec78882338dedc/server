import * as connections from '../../config/connection/connection';
import { Document, Schema, Types } from 'mongoose';

/**
 * @export
 * @interface IContentUser
 * @extends {Document}
 */
export interface IClusterUser {
  id: Types.ObjectId;
  role: string;
  name: string;
}

/**
 * @export
 * @interface IClusterModel
 * @extends {Document}
 */
export interface IClusterModel extends Document {
  _id: Types.ObjectId;
  createdAt?: Date;
  updatedAt?: Date;
  name: string;
  hashedName: string;
  owner: IClusterUser;
  allowedUsers: IClusterUser[];
  location: string;
  totalSize: number;
  convertedTotalSize: string;
  maxSize: number;
  convertedMaxSize: string;
}

const ClusterSchema: Schema = new Schema({
  name: {
    type: String,
    required: true
  },
  hashedName: {
    type: String,
    required: true
  },
  owner: {
    type: Object
  },
  allowedUsers: [
    { 
      _id: {
        type: Types.ObjectId,
        required: true,
      },
      role: {
        type: String,
        required: true,
      },
      name: {
        type: String,
        required: true,
      }
    }
  ],
  location: {
    type: String,
    required: true,
  },
  totalSize: {
    type: Number,
    required: false,
    default: 0
  },
  convertedTotalSize: {
    type: String,
    required: false,
    default: '0 KB'
  },
  maxSize: {
    type: Number,
    required: true,
  },
  convertedMaxSize: {
    type: String, 
    required: false,
  }
}, {
  collection: 'cluster',
  versionKey: false
}).pre('save', function (this: any, next: any): any {
  if (this._doc) {
    const doc: IClusterModel = this._doc;
    const now: Date = new Date();

    if (!doc.createdAt) {
      doc.createdAt = now;
      doc.allowedUsers = [];
    }
    doc.updatedAt = now;
  }
  next();

  return this;
});

export default connections.db.model<IClusterModel>('ClusterModel', ClusterSchema, 'cluster');
