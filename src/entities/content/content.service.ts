import { HttpError } from '../../config/error/index';
import { errors } from '../../config/error/errorsTypes';
import { IContentService, File, GetContentBody } from './content.interface';
import ContentModel, { IContentModel, IContentUser } from './content.model';
import { IAWSService } from '../../services/aws/aws.interface';
import { ManagedUpload } from 'aws-sdk/lib/s3/managed_upload';
import AWSService from '../../services/aws/aws.service';
import UserModel, { IUserModel } from '../user/user.model';
import { Converter } from '../../services/converter/converter.service';
import clusterModel, { IClusterModel } from '../cluster/cluster.model';

/**
 * @export
 * @class ContentService
 * @implements {IUserModelService}
 */
class ContentService implements IContentService {

  /**
   * @private
   * @type IAWSService
   * @memberof ContentService
   */
  private awsService: IAWSService;

  /**
   * Creates an instance of ContentService.
   * @constructor
   * @memberof ContentService
   */
  constructor(
    awsService: IAWSService
  ) {
    this.awsService = awsService;
  }

  /**
   * @async
   * @param {GetContentBody} params
   * @returns {<Promise <boolean>}
   * @memberof ContentService 
   */
  public async getContent(params: GetContentBody): Promise<IContentModel[]> {
    try {
      const condition: any = {
        'owner.userId': params.userId,
        trash: false,
        parentId: params.parentId || null,
      };

      if (!params.parentId && !params.filter) {
        condition.path = '/root';
        delete condition.parentId;
      }

      if (params.filter) this.setFilter(params, condition);
      if (!params.parentId && params.filter) delete condition.parentId;

      return await ContentModel.find(condition).limit(params.limit).skip(params.skip);
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @async
   * @param {IContentModel} content
   * @returns {Promise<IContentModel>}
   * @memberof ContentService
   */
  public async create(content: IContentModel): Promise<IContentModel> {
    try {
      const user: IUserModel = await UserModel.findById(content.owner);
      const cluster: IClusterModel = await clusterModel.findById(content.clusterId);

      if (!user || !cluster) throw errors.notFound;

      content.owner = {
        userId: user._id,
        role: 'owner',
        name: user.fullname
      };

      if (content.parentId) {
        const parentFolder: IContentModel = await ContentModel.findById(content.parentId);
        if (!parentFolder) throw errors.notFound;
        if (parentFolder.clusterId.toString() !== cluster._id.toString()) throw errors.forbidden;

        content.path = `${parentFolder.path}/${parentFolder._id}`;
      } else {
        content.path = '/root';
      }

      return await ContentModel.create(content);
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @async
   * @param {IContentModel} content
   * @param {File} file
   * @returns {Promise<IContentModel>}
   * @memberof ContentService
   */
  public async uploadContent(content: IContentModel, file: File): Promise<IContentModel> {
    try {
      const user: IUserModel = await UserModel.findById(content.owner);
      const cluster: IClusterModel = await clusterModel.findById(content.clusterId);

      if (!user || !cluster) throw errors.notFound;

      content.owner = {
        userId: user._id,
        role: 'owner',
        name: user.fullname
      };

      if (content.parentId) {
        const parentFolder: IContentModel = await ContentModel.findById(content.parentId);
        if (!parentFolder) throw errors.notFound;
        if (parentFolder.clusterId.toString() !== cluster._id.toString()) throw errors.forbidden;

        content.path = `${parentFolder.path}/${parentFolder._id}`;
      } else {
        content.path = '/root';
      }

      cluster.totalSize += file.size;
      cluster.convertedTotalSize = Converter.bytes(cluster.totalSize);
      await cluster.save();

      const uploadInfo: ManagedUpload.SendData | any = await this.awsService.uploadFile(file, cluster);

      return await ContentModel.create(this.configureContent(content, file, uploadInfo));
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @async
   * @param {string} contentId
   * @param {IAllowedUser[]} users
   * @returns {Promise<boolean>}
   * @memberof ContentService
   */
  public async setAccess(contentId: string, users: IContentUser[]): Promise<boolean> {
    try {
      const contentItem: IContentModel = await ContentModel.findById(contentId);

      users.forEach((user: IContentUser) => {
        const hasUser: boolean = contentItem.allowedUsers.some((allowedUser: IContentUser) =>
          allowedUser.userId.toString() === user.userId.toString());

        if (!hasUser) {
          contentItem.allowedUsers.push(user);
          
          return;
        }

        contentItem.allowedUsers = contentItem.allowedUsers.filter((allowedUser: IContentUser): boolean => 
          allowedUser.userId.toString() !== user.userId.toString());
      });
      
      await contentItem.save();

      return true;

    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @async
   * @param {string} userId
   * @param {string[]} content
   * @returns {Promise<IContentModel>}
   * @memberof ContentService
   */
  public async delete(userId: string, content: string[]): Promise<boolean> {
    try {
      const user: IUserModel = await UserModel.findById(userId);
      if (!user) throw errors.notFound;

      const files: IContentModel[] = await ContentModel.find(
        {
          'owner.id': user.id,
          trash: true,
          type: { $ne: 'folder' },
          $or: [
            { _id: { $in: content } },
            { path: { $regex: Converter.getRegexFromArray(content) } }
          ]
        }
      );
      const condition: any = {
        $or: [
          {
            _id: { $in: content },
            'owner.id': user.id,
            trash: true,
          },
          {
            path: { $regex: Converter.getRegexFromArray(content) },
            trash: true,
          }
        ]
      };

      if (files.length) {
        const cluster: IClusterModel = await clusterModel.findById(files[0].clusterId);
        await this.awsService.deleteObjects(cluster, files);
      }

      await ContentModel.deleteMany(condition);

      return true;
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @async
   * @param {IContentModel} content
   * @returns {Promise<IContentModel>}
   * @memberof ContentService
   */
  public async updateOne(content: IContentModel): Promise<IContentModel> {
    try {
      const updatedItem: IContentModel = await ContentModel.findById(content.id);

      if (content.parentId !== updatedItem.parentId) {
        await this.updatePaths(content, updatedItem);
      }

      updatedItem.name = content.name || updatedItem.name;
      updatedItem.description = content.description || updatedItem.description;
      await updatedItem.save();

      return updatedItem;
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @async
   * @param {string} userId
   * @param {string[]} content
   * @param {boolean} restore
   * @returns {Promise<boolean>}
   * @memberof ContentService
   */
  public async trash(userId: string, content: string[], restore: boolean): Promise<boolean> {
    try {
      const user: IUserModel = await UserModel.findById(userId);
      if (!user) throw errors.notFound;

      const condition: any = {
        $or: [
          {
            'owner.id': user.id,
            _id: { $in: content }
          },
          {
            path: {
              $regex: Converter.getRegexFromArray(content),
            }
          }
        ]
      };

      await ContentModel.updateMany(condition, { trash: restore ? false : true });

      return true;
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @async
   * @param {string} userId
   * @param {string} contentId
   * @returns {Promise<boolean>}
   * @memberof ContentService
   */
  public async favorite(userId: string, contentId: string): Promise<boolean> {
    try {
      const user: IUserModel = await UserModel.findById(userId);
      if (!user) throw errors.notFound;

      const content: IContentModel = await ContentModel.findOne({
        _id: contentId,
        $or: [
          { 'owner.userId': userId },
          { allowedUsers: { $elemMatch: { userId } } }
        ],
      });

      if (!content) throw errors.notFound;

      content.favorite = !content.favorite;
      content.save();

      return true;
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @private
   * @param {string} id
   * @param {File} file
   * @param {any} uploadInfo
   * @returns {IContentModel}
   * @memberof ContentService
   */
  private configureContent(content: IContentModel, file: File, uploadInfo: any): IContentModel {
    return new ContentModel({
      name: content.name || file.name,
      description: content.description,
      owner: content.owner,
      parentId: content.parentId || null,
      clusterId: content.clusterId || null,
      type: file.mimetype,
      path: content.path,
      resource: {
        link: uploadInfo.Location,
        originalName: file.name,
        key: uploadInfo.key,
        convertedSize: Converter.bytes(file.size),
        size: file.size
      },
    });
  }

  /**
   * @private
   * @param {GetContentBody} params
   * @param {any} condition
   * @returns {string}
   * @memberof ContentService
   */
  private setFilter(params: GetContentBody, condition: any): void {
    if (params.filter.owner) {
      condition['owner.name'] = { $regex: `^${params.filter.owner}` };
    }

    if (params.filter.name) {
      condition.name = { $regex: `^${params.filter.name}` };
    }

    if (params.filter.description) {
      condition.description = { $regex: `^${params.filter.description}` };
    }
  }

  /**
   * @private
   * @async
   * @param {IContentModel} content
   * @returns {Promise<IContentModel>}
   * @memberof ContentService
   */
  private async updatePaths(content: IContentModel, updatedItem: IContentModel): Promise<void> {
    if (content.parentId) {
      const parent: IContentModel = await ContentModel.findById(content.parentId);
      if (!parent) throw errors.notFound;

      updatedItem.path = `${parent.path}/${parent._id}`;
      updatedItem.parentId = parent._id;
    } else {
      updatedItem.path = '/root';
      updatedItem.parentId = null;
    }

    if (updatedItem.type === 'folder') {
      const regex: string = `.*${updatedItem._id}*.`;
      const contents: IContentModel[] = await ContentModel.find({ path: { $regex: regex } });

      contents.forEach((child: IContentModel) => {
        const afterInsert: string = child.path.substring(child.path.indexOf(updatedItem._id.toHexString()), child.path.length);
        child.path = `${updatedItem.path}/${afterInsert}`;
        child.save();
      });
    }
  }
}

export default new ContentService(AWSService);
