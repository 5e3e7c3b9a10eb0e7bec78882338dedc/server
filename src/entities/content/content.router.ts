import { authenticate } from 'passport';
import { Router } from 'express';
import ContentController from './content.controller';
import ContentValidation from './content.validation';

/**
 * @export
 * @class ContentRouter
 */
class ContentRouter {
  public router: Router;

  /**
   * Creates an instance of ContentRouter.
   * @memberof ContentRouter
   */
  constructor() {
    this.router = Router();
    this.routes();
  }

  /**
   * @memberof ContentRouter
   */
  public routes(): void {

    this.router.get('/download/:contentId',
      authenticate('bearer', { session: false }), 
      ContentValidation.download.bind(ContentValidation),
      ContentController.download.bind(ContentController));

    this.router.post('/', 
      authenticate('bearer', { session: false }), 
      ContentValidation.getContent.bind(ContentValidation),
      ContentController.get.bind(ContentController));

    this.router.post('/access', 
      authenticate('bearer', { session: false }), 
      ContentValidation.access.bind(ContentValidation),
      ContentController.setAccess.bind(ContentController));

    this.router.post('/create', 
      authenticate('bearer', { session: false }),
      ContentValidation.createContent.bind(ContentValidation),
      ContentController.create.bind(ContentController));

    this.router.post('/upload', 
      authenticate('bearer', { session: false }), 
      ContentValidation.createContent.bind(ContentValidation),
      ContentController.upload.bind(ContentController));

    this.router.post('/trash', 
      authenticate('bearer', { session: false }), 
      ContentValidation.trash.bind(ContentValidation),
      ContentController.trash.bind(ContentController));

    this.router.post('/favorite', 
      authenticate('bearer', { session: false }), 
      ContentValidation.favorite.bind(ContentValidation),
      ContentController.favorite.bind(ContentController));

    this.router.put('/update',  
      authenticate('bearer', { session: false }), 
      ContentValidation.updateContent.bind(ContentValidation),
      ContentController.update.bind(ContentController));

    this.router.post('/delete', 
      authenticate('bearer', { session: false }), 
      ContentValidation.trash.bind(ContentValidation),
      ContentController.delete.bind(ContentController));
  }
}

export default new ContentRouter();
