import * as connections from '../../config/connection/connection';
import { Document, Schema, Types } from 'mongoose';

/**
 * @export
 * @interface IContentUser
 * @extends {Document}
 */
export interface IContentUser {
  userId: Types.ObjectId;
  role: string;
  name: string;
}

/**
 * @export
 * @interface IResource
 * @extends {Document}
 */
export interface IResource {
  originalName: string;
  key: string;
  link: string;
  convertedSize: string;
  size: number;
}

/**
 * @export
 * @interface ContentModel
 * @extends {Document}
 */
export interface IContentModel extends Document {
  _id: Types.ObjectId;
  createdAt?: Date;
  updatedAt?: Date;
  name: string;
  description: string;
  owner: IContentUser;
  type: string;
  parentId?: Types.ObjectId;
  clusterId?: Types.ObjectId;
  path: string;
  resource?: IResource;
  allowedUsers?: IContentUser[];
  trash?: boolean;
  favorite?: boolean;
}

const ContentSchema: Schema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: false,
  },
  owner: { 
    userId: {
      type: Types.ObjectId,
      required: true,
    },
    role: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    }
  },
  type: {
    type: String,
    required: true
  },
  parentId: {
    type: Types.ObjectId,
    required: false,
  },
  clusterId: {
    type: Types.ObjectId,
    required: false,
  },
  path: {
    type: String,
    required: true,
  },
  resource: {
    type: Object,
    required: false
  },
  allowedUsers: [
    { 
      userId: {
        type: Types.ObjectId,
        required: true,
      },
      role: {
        type: String,
        required: true,
      },
      name: {
        type: String,
        required: true,
      }
    }
  ],
  trash: {
    type: Boolean,
    required: false,
    default: false
  },
  favorite: {
    type: Boolean,
    required: false,
    default: false
  }
}, {
  collection: 'content',
  versionKey: false
}).pre('save', function (this: any, next: any): any {
  if (this._doc) {
    const doc: IContentModel = this._doc;
    const now: Date = new Date();

    if (!doc.createdAt) {
      doc.createdAt = now;
      doc.allowedUsers = [];
      if (!doc.parentId) {
        doc.parentId = null;
      }
    }
    doc.updatedAt = now;
  }
  next();

  return this;
});

export default connections.db.model<IContentModel>('ContentModel', ContentSchema, 'content');
