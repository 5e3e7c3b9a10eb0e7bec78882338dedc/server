import { HttpError } from '../../config/error/index';
import { NextFunction, Request, Response } from 'express';
import { IContentModel } from './content.model';
import { IContentService } from './content.interface';
import ContentService from './content.service';
import { Stream } from 'stream';
import { IAWSService } from '../../services/aws/aws.interface';
import AWSService from '../../services/aws/aws.service';

class ContentController {

  /**
   * @private
   * @type {IContentService}
   * @memberof ContentController
   */
  private contentService: IContentService;

  /**
   * @private
   * @type {IAWSService}
   * @memberof ContentController
   */
  private awsService: IAWSService;

  /**
   * Creates an instance of ContentController.
   * @memberof ContentController
   */
  constructor(
    contentService: IContentService,
    awsService: IAWSService,
  ) {
    this.contentService = contentService;
    this.awsService = awsService;
  }

  /**
   * @async
   * @param {Request} req
   * @param {Response} res
   * @returns {Promise < Response >}
   * @memberof ContentController
   */
  public async get(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const content: IContentModel[] = await this.contentService.getContent(req.body);

      return res.status(200).json({ content, success: true });
    } catch (error) {
      next(new HttpError(error.status, error.message));
    }
  }

  
  /**
   * @async
   * @param {Request} req
   * @param {Response} res
   * @returns {Promise < Response >}
   * @memberof ContentController
   */
  public async download(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const file: Stream = await this.awsService.downloadFile(req.params.contentId);
      file.pipe(res);

      return res.status(200);
    } catch (error) {
      next(new HttpError(error.status, error.message));
    }
  }

  /**
 * @async
 * @param {Request} req
 * @param {Response} res
 * @returns {Promise < Response >}
 * @memberof ContentController
 */
  public async setAccess(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const success: boolean = await this.contentService.setAccess(req.body.contentId, req.body.users);

      return res.status(200).json({ success });
    } catch (error) {
      next(new HttpError(error.status, error.message));
    }
  }

  /**
   * @async
   * @param {Request} req
   * @param {Response} res
   * @returns {Promise < Response >}
   * @memberof ContentController
   */
  public async upload(req: Request | any, res: Response, next: NextFunction): Promise<Response> {
    try {
      const content: IContentModel = await this.contentService.uploadContent(req.body, req.files.file);

      return res.status(200).json({ content, success: true });
    } catch (error) {
      next(new HttpError(error.status, error.message));
    }
  }

  /**
   * @async
   * @param {Request} req
   * @param {Response} res
   * @returns {Promise < Response >}
   * @memberof ContentController
   */
  public async create(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const content: IContentModel = await this.contentService.create(req.body);

      return res.status(200).json({ content, success: true });
    } catch (error) {
      next(new HttpError(error.status, error.message));
    }
  }

  /**
   * @async
   * @param {Request} req
   * @param {Response} res
   * @returns {Promise < Response >}
   * @memberof ContentController
   */
  public async delete(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const success: boolean = await this.contentService.delete(req.body.userId ,req.body.content);

      return res.status(200).json({ success });
    } catch (error) {
      next(new HttpError(error.status, error.message));
    }
  }

  /**
 * @async
 * @param {Request} req
 * @param {Response} res
 * @returns {Promise < Response >}
 * @memberof ContentController
 */
  public async update(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const content: IContentModel = await this.contentService.updateOne(req.body);

      return res.status(200).json({ content, success: true });
    } catch (error) {
      next(new HttpError(error.status, error.message));
    }
  }

  /**
   * @async
   * @param {Request} req
   * @param {Response} res
   * @returns {Promise < Response >}
   * @memberof ContentController
   */
  public async trash(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const success: boolean = await this.contentService.trash(req.body.userId, req.body.content, req.body.restore);

      return res.status(200).json({ success });
    } catch (error) {
      next(new HttpError(error.status, error.message));
    }
  }

    /**
   * @async
   * @param {Request} req
   * @param {Response} res
   * @returns {Promise < Response >}
   * @memberof ContentController
   */
  public async favorite(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const success: boolean = await this.contentService.favorite(req.body.userId, req.body.contentId);

      return res.status(200).json({ success });
    } catch (error) {
      next(new HttpError(error.status, error.message));
    }
  }
}


export default new ContentController(ContentService, AWSService);
