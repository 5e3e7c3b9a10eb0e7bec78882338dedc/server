import { IContentModel, IContentUser } from './content.model';
import { UploadedFile } from 'express-fileupload';

export interface GetContentBody extends Object {
  userId: string;
  parentId?: string;
  limit?: number;
  skip?: number;

  filter?: {
    name?: string,
    description?: string,
    owner?: string,
  };

  sort?: {
    owner?: string,
    createDate?: string,
    updateDate?: string,
  };
}

export interface File extends UploadedFile {
  size: number;
}

export interface IBucketInterface {
  Bucket: string;
  Key: string;
  Body: Buffer;
}

/**
 * @export
 * @interface IContentService
 */
export interface IContentService {
  /**
   * @async
   * @param {GetContentBody} params
   * @returns {Promise<IContentModel[]>}
   * @memberof IContentService
   */
  getContent(params: GetContentBody): Promise<IContentModel[]>;

  /**
   * @async
   * @param {IContentModel} content
   * @param {File} file
   * @returns {Promise<IContentModel>}
   * @memberof IContentService
   */
  uploadContent(content: IContentModel, file: File): Promise<IContentModel>;

  /**
   * @async
   * @param {string} contentId
   * @param {IAllowedUser[]} users
   * @returns {Promise<boolean>}
   * @memberof IContentService
   */
  setAccess(contentId: string, users: IContentUser[]): Promise<boolean>;

  /**
   * @async
   * @param {IContentModel} content
   * @returns {Promise<IContentModel>}
   * @memberof IContentService
   */
  create(content: IContentModel): Promise<IContentModel>;

  /**
   * @async
   * @param {string} userId
   * @param {string[]} content
   * @returns {Promise<boolean>}
   * @memberof IContentService
   */
  delete(userId: string, content: string[]): Promise<boolean>;

  /**
   * @async
   * @param {IContentModel} content
   * @returns {Promise<IContentModel>}
   * @memberof IContentService
   */
  updateOne(content: IContentModel): Promise<IContentModel>;

  /**
   * @async
   * @param {string} userId
   * @param {string[]} content
   * @param {boolean} restore
   * @returns {Promise<boolean>}
   * @memberof IContentService
   */
  trash(userId: string, content: string[], restore: boolean): Promise<boolean>;

  /**
   * @async
   * @param {string} userId
   * @param {string} contentId
   * @returns {Promise<boolean>}
   * @memberof IContentService
   */
  favorite(userId: string, contentId: string): Promise<boolean>;
}
