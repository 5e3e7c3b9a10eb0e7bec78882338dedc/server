import * as Joi from 'joi';
import { Types } from 'mongoose';
import { IContentModel, IContentUser } from './content.model';
import { NextFunction, Request, Response } from 'express';
import { errors } from '../../config/error/errorsTypes';
import HttpError from '../../config/error';
import { GetContentBody } from './content.interface';

/**
 * @export
 * @class JoiSchema
 */
class ContentValidation {
  customJoi: any;

  /**
   * @static
   * @type {string}
   * @memberof JoiSchema
   */
  readonly messageObjectId: string =
    'Argument passed in must be a single String of 12 bytes or a string of 24 hex characters';

  /**
   * Creates an instance of Schema.
   * @memberof JoiSchema
   */
  constructor() {
    this.customJoi = Joi.extend({
      name: 'objectId',
      language: {
        base: this.messageObjectId
      },
      pre(value: any, state: Joi.State, options: Joi.ValidationOptions): any {
        if (!Types.ObjectId.isValid(value)) {
          return this.createError(
            'objectId.base', {
              value
            },
            state,
            options
          );
        }

        return value; // Keep the value as it was
      }
    });
  }

  /**
   * @param {Request} req
   * @param {Response} res
   * @returns void
   * @memberof ContentValidation
   */
  public getContent(req: Request, res: Response, next: NextFunction): void {
    try {
      const body: GetContentBody = req.body;

      if (req.query.parentId) body.parentId = req.query.parentId;

      const schema: Joi.Schema = Joi.object().keys({
        userId: this.customJoi.objectId().required(),
        parentId: this.customJoi.objectId(),
        limit: Joi.number(),
        skip: Joi.number(),
        filter: {
          name: Joi.string().max(50),
          description: Joi.string().max(200),
          owner: Joi.string().max(50)
        },
        sort: {
          owner: Joi.string().max(4),
          createDate: Joi.string().max(4),
          updateDate: Joi.string().max(4),
        }
      });
      const validate: Joi.ValidationResult<GetContentBody> = Joi.validate(body, schema);

      if (validate.error) throw errors.getCustomError(400, validate.error.message);
      
      next();
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @param {Request} req
   * @param {Response} res
   * @returns void
   * @memberof ContentValidation
   */
  public download(req: Request, res: Response, next: NextFunction): void {
    try {
      const body: { contentId: string } = { contentId: req.params.contentId };
      const schema: Joi.Schema = Joi.object().keys({
        contentId: this.customJoi.objectId().required(),
      });
      const validate: Joi.ValidationResult<{ contentId: string }> = Joi.validate(body, schema);

      if (validate.error) throw errors.getCustomError(400, validate.error.message);
      
      next();
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @param {Request} req
   * @param {Response} res
   * @returns void
   * @memberof ContentValidation
   */
  public access(req: Request, res: Response, next: NextFunction): void {
    try {
      const schema: Joi.Schema = Joi.object().keys({
        userId: this.customJoi.objectId().required(),
        role: Joi.string().max(10).required(),
        name: Joi.string().min(4).max(40).required(),
      });
      req.body.users.forEach((user: IContentUser): void => {
        const validate: Joi.ValidationResult<IContentUser> = Joi.validate(user, schema);
        if (validate.error) throw errors.getCustomError(400, validate.error.message);
      });
      
      next();
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @param {Request} req
   * @param {Response} res
   * @returns void
   * @memberof ContentValidation
   */
  public createContent(req: Request, res: Response, next: NextFunction): void {
    try {
      const schema: Joi.Schema = Joi.object().keys({
        owner: this.customJoi.objectId().required(),
        clusterId: this.customJoi.objectId().required(),
        parentId: this.customJoi.objectId(),
        name: req.body.type === 'folder' ? Joi.string().max(50).required() : Joi.string().max(50),
        description: Joi.string().max(200),
        type: Joi.string(),
        path: Joi.string(),
      });

      const validate: Joi.ValidationResult<IContentModel> = Joi.validate(req.body, schema);

      if (validate.error) throw errors.getCustomError(400, validate.error.message);
      
      next();
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @param {Request} req
   * @param {Response} res
   * @returns void
   * @memberof ContentValidation
   */
  public updateContent(req: Request, res: Response, next: NextFunction): void {
    try {
      const schema: Joi.Schema = Joi.object().keys({
        id: this.customJoi.objectId().required(),
        name: Joi.string().max(50),
        description: Joi.string().max(200),
        parentId: this.customJoi.objectId(),
      });
      const validate: Joi.ValidationResult<IContentModel> = Joi.validate(req.body, schema);

      if (validate.error) throw errors.getCustomError(400, validate.error.message);
      
      next();
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @param {Request} req
   * @param {Response} res
   * @returns void
   * @memberof ContentValidation
   */
  public trash(req: Request, res: Response, next: NextFunction): void {
    try {
      const schemaUserId: Joi.Schema = Joi.object().keys({
        userId: this.customJoi.objectId().required(),
        content: Joi.array().min(1),
        restore: Joi.boolean(),
      });
            
      const validate: Joi.ValidationResult<{ userId: string, content: string[], restore: boolean }> = Joi.validate(req.body, schemaUserId);
      if (validate.error) throw errors.getCustomError(400, validate.error.message);


      const schema: Joi.Schema = Joi.object().keys({
        id: this.customJoi.objectId().required(),
      });

      req.body.content.forEach((id: string): void => {
        const validate: Joi.ValidationResult<{ id: string }> = Joi.validate({ id }, schema);
        if (validate.error) throw errors.getCustomError(400, validate.error.message);
      });


      next();
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @param {Request} req
   * @param {Response} res
   * @returns void
   * @memberof ContentValidation
   */
  public delete(req: Request, res: Response, next: NextFunction): void {
    try {
      const body: { contentId: string } = { contentId: req.params.contentId };
      const schema: Joi.Schema = Joi.object().keys({
        contentId: this.customJoi.objectId().required(),
      });
      const validate: Joi.ValidationResult<{ contentId: string }> = Joi.validate(body, schema);

      if (validate.error) throw errors.getCustomError(400, validate.error.message);

      next();
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

    /**
   * @param {Request} req
   * @param {Response} res
   * @returns void
   * @memberof ContentValidation
   */
  public favorite(req: Request, res: Response, next: NextFunction): void {
    try {
      const schema: Joi.Schema = Joi.object().keys({
        userId: this.customJoi.objectId().required(),
        contentId: this.customJoi.objectId().required(),
      });
      const validate: Joi.ValidationResult<{ userId: string, contentId: string }> = Joi.validate(req.body, schema);

      if (validate.error) throw errors.getCustomError(400, validate.error.message);

      next();
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }
}

export default new ContentValidation();
