import * as connections from '../../config/connection/connection';
import * as crypto from 'crypto';
import { Document, Schema, Types } from 'mongoose';
import { prop, Typegoose, ModelType, InstanceType } from 'typegoose';

/**
 * @export
 * @interface ICredentials
 */
export interface ICredentials {
  email: string;
  password: string;
}

/**
 * @export
 * @interface IToken
 */
export interface IToken {
  hash: string;
  expires: number;
}

/**
 * @export
 * @interface IUserModel
 * @extends {Document}
 */
export interface IUserModel extends Document {
  _id: Types.ObjectId;
  createdAt?: Date;
  updatedAt?: Date;
  credentials: ICredentials;
  username: string;
  fullname: string;
  avatar: string;
  salt: string;
  phone: string;
  token: IToken;
  isActive: boolean;

  checkPassword(password: string): boolean;
  encryptPassword(password: string): string;
  getToken(): IToken;
}

const UserSchema: Schema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  credentials: {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
  },
  fullname: {
    type: String,
    required: true,
  },
  avatar: {
    type: String,
    required: false,
  },
  salt: {
    type: String,
    required: false,
  },
  phone: {
    type: String,
    required: false,
  },
  token: {
    hash: {
      type: String, 
      required: false,
    },
    expires: {
      type: Number,
      required: false,
    }
  },
  isActive: {
    type: Boolean,
    required: false,
  }
}, {
    collection: 'user',
    versionKey: false
  }).pre('save', function (this: any, next: any): any {
    if (this._doc) {
      const doc: IUserModel = this._doc;
      const now: Date = new Date();

      if (!doc.createdAt) {
        doc.createdAt = now;
        doc.salt = crypto.randomBytes(16).toString('hex');
        doc.credentials.password = this.encryptPassword(doc.credentials.password);
        doc.isActive = false;
      }
      doc.updatedAt = now;
    }
    next();

    return this;
  });

class User extends Typegoose {

  @prop() credentials?: ICredentials;
  @prop() salt?: string;
  
  /**
   * @param {string} password
   * @returns {boolean}
   * @memberof User 
   */
  checkPassword(password: string): boolean {  
    return this.encryptPassword(password) === this.credentials.password;
  }

  /**
   * @private
   * @param {string} password
   * @returns {string}
   * @memberof User 
   */
  private encryptPassword(password: string): string {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
  }

  /**
   * @returns {IToken}
   * @memberof User 
   */
  getToken(): IToken {
    return {
      hash: crypto.randomBytes(48).toString('hex'),
      expires: Date.now() + 172800000,
    };
  }
}

UserSchema.loadClass(User);

export default connections.db.model<IUserModel>('UserModel', UserSchema, 'user');
