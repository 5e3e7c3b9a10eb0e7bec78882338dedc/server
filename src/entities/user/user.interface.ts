import { IUserModel, ICredentials, IToken } from './user.model';

/**
 * @export
 * @interface IUserService
 */
export interface IUserService {

  /**
   * @async
   * @param {string} appID
   * @returns {<Promise <IUserModel>}
   * @memberof IUserService
   */
  authenticate(appID: string): Promise<IUserModel>;

  /**
   * @async
   * @param {string} hash
   * @returns {Promise<boolean>}
   * @memberof IUserService
   */
  checkAccess(hash: string): Promise<boolean>;
}
