import UserController from './user.controller';
import * as passport from 'passport';
import { Router } from 'express';

/**
 * @export
 * @class UserRouter
 */
class UserRouter {
  public router: Router;

  /**
   * Creates an instance of UserRouter.
   * @memberof UserRouter
   */
  constructor() {
    this.router = Router();
    this.routes();
  }

  /**
   * @memberof UserRouter
   */
  public routes(): void {
    // TODO: test auth
    this.router.get('/id/:appID', UserController.login.bind(UserController));
    // End of test auth
  }
}

export default new UserRouter();
