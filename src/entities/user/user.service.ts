import * as Joi from 'joi';
import UserModel, { IUserModel, ICredentials, IToken } from './user.model';
import UserValidation from './user.validation';
import { IUserService } from './user.interface';
import { Types } from 'mongoose';
import { HttpError } from '../../config/error/index';
import { errors } from '../../config/error/errorsTypes';

/**
 * @export
 * @class UserService
 * @implements {IUserModelService}
 */
class UserService implements IUserService {
  
  /**
   * @async
   * @param {string} hash
   * @returns {<Promise <boolean>}
   * @memberof UserService 
   */
  public async checkAccess(hash: string): Promise<boolean> {
    try {
      const user: IUserModel = await UserModel.findOne({ 'token.hash': hash });
      if (!user) return false;

      return true;
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @async
   * @param {string} appID
   * @returns {<Promise <IUserModel>}
   * @memberof UserService 
   */
  public async authenticate(appID: string): Promise<IUserModel> {
    try {

      /*const validate: Joi.ValidationResult<ICredentials> = UserValidation.authenticate(credentials);

      if (validate.error) {
        throw errors.getCustomError(400, validate.error.message);
      }

      const user: IUserModel = await UserModel.findOne({ 'credentials.email': credentials.email });

      if (!user || !user.checkPassword(credentials.password)) throw errors.authFailure;
      if (!user.isActive) throw errors.inactiveAccount;*/

      // TODO: test find user
      const user: IUserModel = await UserModel.findById(appID);
      // end of TODO
      user.token = user.getToken();
      await user.save();

      return user;
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }
}

export default new UserService();
