import UserService from './user.service';
import { HttpError } from '../../config/error/index';
import { IUserModel, IToken } from './user.model';
import { IUserService } from './user.interface';
import { NextFunction, Request, Response } from 'express';
import { IMailService, EmailRenderer } from '../../services/mail/mail.interface';
import MailService from '../../services/mail/mail.service';

/**
 * @export
 * @class UserController
 */
class UserController {

  /**
   * @private
   * @type {IUserModelService}
   * @memberof UserController
   */
  private userService: IUserService;

  /**
 * @private
 * @type {IMailService}
 * @memberof UserController
 */
  private mailService: IMailService;

  /**
   * Creates an instance of UserController.
   * @param {IUserModelService} repository
   * @memberof UserController
   */
  constructor(
    userService: IUserService,
    mailService: IMailService,
  ) {
    this.userService = userService;
    this.mailService = mailService;
  }

  /**
   * @async
   * @param {Request} req
   * @param {Response} res
   * @returns {Promise < Response >}
   * @memberof UserController
   */
  public async login(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      // TODO: test auth by userId (for development)
      // const accessToken: IToken = await this.userService.authenticate(req.body);

      const user: IUserModel = await this.userService.authenticate(req.params.appID); 

      return res.status(200).json(user);
    } catch (error) {
      next(new HttpError(error.status, error.message));
    }
  }
}

export default new UserController(UserService, MailService);
