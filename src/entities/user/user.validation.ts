import * as Joi from 'joi';
import { IUserModel, ICredentials } from './user.model';
import { Types } from 'mongoose';

/**
 * @export
 * @class JoiSchema
 */
class UserValidation {
  customJoi: any;

  /**
   * @static
   * @type {string}
   * @memberof JoiSchema
   */
  readonly messageObjectId: string =
    'Argument passed in must be a single String of 12 bytes or a string of 24 hex characters';

  /**
   * Creates an instance of Schema.
   * @memberof JoiSchema
   */
  constructor() {
    this.customJoi = Joi.extend({
      name: 'objectId',
      language: {
        base: this.messageObjectId
      },
      pre(value: any, state: Joi.State, options: Joi.ValidationOptions): any {
        if (!Types.ObjectId.isValid(value)) {
          return this.createError(
            'objectId.base', {
              value
            },
            state,
            options
          );
        }

        return value; // Keep the value as it was
      }
    });
  }

  /**
   * @param {IUserModel} credentials
   * @returns {Joi.ValidationResult<IUserModel >}
   * @memberof UserValidation
   */
  public authenticate(credentials: ICredentials): Joi.ValidationResult<ICredentials> {
    const schema: Joi.Schema = Joi.object().keys({
      email: Joi.string().email({
        minDomainAtoms: 2
      }).required().min(5).max(40),
      password: Joi.string().required().min(8).max(40)
    });

    return Joi.validate(credentials, schema);
  }

  /**
   * @param {IUserModel} params
   * @returns {Joi.ValidationResult<IUserModel >}
   * @memberof UserValidation
   */
  public createUser(params: IUserModel): Joi.ValidationResult<IUserModel> {
    const schema: Joi.Schema = Joi.object().keys({
      username: Joi.string().min(8).max(40).required(),
      fullname: Joi.string().min(4).max(40).required(),
      phone: Joi.string().max(15),
      credentials: Joi.object({
        email: Joi.string().email({
          minDomainAtoms: 2
        }).required().min(5).max(40),
        password: Joi.string().min(8).max(40).required(),
      }),
    });

    return Joi.validate(params, schema);
  }

  /**
   * @param {{ id: string }} body
   * @returns {Joi.ValidationResult<{ id: string }>}
   * @memberof UserValidation
   */
  public getUser(body: { id: string }): Joi.ValidationResult<{ id: string }> {
    const schema: Joi.Schema = Joi.object().keys({
      id: this.customJoi.objectId().required()
    });

    return Joi.validate(body, schema);
  }

  /**
   * @param {{ email: string }} body
   * @returns {Joi.ValidationResult<{ email: string }>}
   * @memberof UserValidation
   */
  public email(body: { email: string }): Joi.ValidationResult<{ email: string }> {
    const schema: Joi.Schema = Joi.object().keys({
      email: Joi.string().email({
        minDomainAtoms: 2
      }).required().min(5).max(40),
    });

    return Joi.validate(body, schema);
  }
  
  /**
   * @param {IUserModel} user
   * @returns {Joi.ValidationResult<IUserModel>}
   * @memberof UserValidation
   */
  public updatePassword(user: { id: string, password: string }): Joi.ValidationResult<{ id: string, password: string }> {
    const schema: Joi.Schema = Joi.object().keys({
      id: this.customJoi.objectId().required(),
      password: Joi.string().required().min(8).max(40)
    });

    return Joi.validate(user, schema);
  }
}

export default new UserValidation();
