import * as crypto from 'crypto';

/**
 * @export
 * @class Converter
 * @implements {IConverterService}
 */
export class Converter {

  /**
   * @public
   * @static
   * @param {number} bytes
   * @returns {string}
   */
  public static bytes(bytes: number): string {
    if (bytes === 0) return '0 Bytes';

    const multiplicity: number = 1024;
    const sizeTags: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const tag: number = Math.floor(Math.log(bytes) / Math.log(multiplicity));

    return Math.round(bytes / Math.pow(multiplicity, tag)) + ' ' + sizeTags[tag];
  }

  /**
   * @public
   * @static
   * @param {number} bytes
   * @returns {string}
   */
  public static hashString(str: string, salt?: string): string {
    return crypto.createHmac('sha1', salt || Date.now().toString()).update(str).digest('hex');
  }

  /**
   * @public
   * @static
   * @param {string[]} arr
   * @param {string} type
   * @returns {string}
   */
  public static getRegexFromArray(arr: string[], type?: string): string {
     
    return arr.reduce((previousValue: string, currentValue: string, currentIndex: number): string => {
      if (currentIndex === 1) {
        previousValue = `.*${previousValue}*.`;
      }

      return `${previousValue}|.*${currentValue}*.`;
    }); 
  }
}
