import { IUserModel } from '../../entities/user/user.model';
import { Transporter, SentMessageInfo } from 'nodemailer';

/**
 * @exports
 * @interface EmailRenderer
 */
export interface EmailRenderer {
  html: string;
  text: string;
  subject: string;
  deeplink: string;
  success: boolean;
}

/**
 * @export
 * @interface IMailOptions
 */
export interface IMailOptions {
  from: string;
  to: string;
  subject: string;
  html: string;
}

/**
 * @export
 * @interface IUserService
 */
export interface IMailService {

  transporter: Transporter;
    
  /**
   * @async
   * @param {IUserModel} IUserModel
   * @returns {Promise<SentMessageInfo>}
   * @memberof IMailService
   */
  sendVerificationEmail(user: IUserModel): Promise<EmailRenderer>;

  /**
   * @async
   * @param {IUserModel} IUserModel
   * @returns {Promise<SentMessageInfo>}
   * @memberof IMailService
   */
  sendResetPasswordEmail(user: IUserModel): Promise<SentMessageInfo>;
}
