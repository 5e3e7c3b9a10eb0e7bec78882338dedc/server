import * as path from 'path';
import { Transporter, createTransport, SentMessageInfo } from 'nodemailer';
import { IMailService, IMailOptions, EmailRenderer } from './mail.interface';
import { IUserModel } from '../../entities/user/user.model';
import { envConfig } from '../../config/env/development';

const EmailTemplate: any = require('email-templates').EmailTemplate;

/**
 * @export
 * @class EmailService
 * @implements {IUserModelService}
 */
class MailService implements IMailService {
  transporter: Transporter;

  /**
   * Creates an instance of MailService.
   * @memberof MailService
   */
  constructor() {
    this.transporter = createTransport({
      service: envConfig.EMAIL.SERVICE,
      auth: {
        user: envConfig.EMAIL.LOGIN,
        pass: envConfig.EMAIL.PASS
      }
    });
  }

  /**
   * @async
   * @param {IUserModel} IUserModel
   * @returns {boolean}
   * @memberof MailService
   */
  async sendVerificationEmail(user: IUserModel): Promise<EmailRenderer> {
    try {
      const templateDir: string = path.join(__dirname, '../../', 'templates/confirm/');
      const template: any = new EmailTemplate(templateDir);

      const params: any = {
        fullname: user.fullname, 
        link: `${envConfig.app.client}/confirm?user_id=${user._id}` 
      };
      
      return await template.render(params, (err: Error, temp: EmailRenderer) => {
        if (err) throw new Error(err.message);

        const mailOptions: IMailOptions = this.getMailConfig('Confirm your mail address', user.credentials.email, temp.html);
        this.transporter.sendMail(mailOptions);
      });
    } catch (error) {
      throw new Error(error);
    }
  }

  /**
   * @async
   * @param {IUserModel} IUserModel
   * @returns {Promise<SentMessageInfo>}
   * @memberof MailService
   */
  async sendResetPasswordEmail(user: IUserModel): Promise<any> {
    try {
      const templateDir: string = path.join(__dirname, '../../', 'templates/reset-password/');
      const template: any = new EmailTemplate(templateDir);

      const params: any = {
        fullname: user.fullname, 
        link: `${envConfig.app.client}/reset?user_id=${user._id}` 
      };
      
      return await template.render(params, (err: Error, temp: EmailRenderer) => {
        if (err) throw new Error(err.message);

        const mailOptions: IMailOptions = this.getMailConfig('Reset your passwprd', user.credentials.email, temp.html);
        this.transporter.sendMail(mailOptions);
      });
    } catch (error) {
      throw new Error(error);
    }
  }

    /**
   * @private
   * @param {string} subject
   * @param {string} email
   * @param {string} html
   * @returns {Promise<IMailOptions>}
   * @memberof MailService
   */
  private getMailConfig(subject: string, email: string, html: string): IMailOptions {
    return {
      subject,
      html,
      from: envConfig.mail.from,
      to: email,
    };
  }
}

export default new MailService();
