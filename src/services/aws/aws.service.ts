
import { HttpError } from '../../config/error/index';
import { errors } from '../../config/error/errorsTypes';
import { UploadedFile } from 'express-fileupload';
import { S3 } from 'aws-sdk';
import { IAWSService, AWSObject } from './aws.interface';
import { IBucketInterface } from '../../entities/content/content.interface';
import { ManagedUpload } from 'aws-sdk/lib/s3/managed_upload';
import ContentModel, { IContentModel } from '../../entities/content/content.model';
import { Stream } from 'stream';
import { envConfig } from '../../config/env/development';
import { IClusterModel } from '../../entities/cluster/cluster.model';
import { Converter } from '../../services/converter/converter.service';

/**
 * @export
 * @class AWSService
 * @implements {IAWSService}
 */
class AWSService implements IAWSService {

  /**
   * @private
   * @type S3
   * @memberof AWSService
   */
  private s3: S3;

  /**
   * Creates an instance of ContentService.
   * @constructor
   * @memberof AWSService
   */
  constructor() {
    this.s3 = new S3({
      accessKeyId: envConfig.AWS.ACCESS_KEY,
      secretAccessKey: envConfig.AWS.SECRET_KEY,
    });
  }

  /**
   * @async
   * @param {UploadedFile} file
   * @param {IClusterModel} cluster
   * @returns {Promise<ManagedUpload.SendData | any>}
   * @memberof AWSService
   */
  public async uploadFile(file: UploadedFile, cluster: IClusterModel): Promise<ManagedUpload.SendData | any> {
    try {
      const fileName: string = `${Converter.hashString(file.name)}-${file.name}`;
      const key: string = `${cluster.hashedName}/${fileName}`;

      const params: IBucketInterface = {
        Bucket: AWSService.getBucketConfig(cluster).usedBucked,
        Key: key,
        Body: file.data
      };

      return await this.s3.upload(params).promise().catch(() => { errors.serverError; });
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @async
   * @param {IClusterModel} cluster
   * @param {IContentModel[]} contents
   * @returns {Promise<string>}
   * @memberof AWSService
   */
  public async deleteObjects(cluster: IClusterModel, contents?: IContentModel[]): Promise<any> {
    try {
      let objectsKeys: { Key: string }[] = null;
      if (contents && contents.length) {
        objectsKeys = contents.map((item: IContentModel): { Key: string } => {
          return {
            Key: item.resource.key,
          };
        });
      } else {
        objectsKeys = [{ Key: `${cluster.hashedName}/` }];
      }

      return await this.s3.deleteObjects({
        Bucket: AWSService.getBucketConfig(cluster).usedBucked,
        Delete: {
          Objects: objectsKeys
        }
      }).promise();
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @async
   * @param {string} id
   * @returns {Promise<Stream>}
   * @memberof AWSService
   */
  public async downloadFile(id: string): Promise<Stream> {
    try {
      const content: IContentModel = await ContentModel.findById(id);
      if (!content) throw errors.notFound;

      return await this.s3
        .getObject({ Bucket: process.env.AWS_BUCKET_NAME, Key: content.resource.key })
        .createReadStream()
        .on('error', (err) => { throw errors.serverError; });

    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @async
   * @param {IClusterModel} cluster
   * @returns {Promise<string>}
   * @memberof AWSService
   */
  public async createCluster(cluster: IClusterModel): Promise<string> {
    try {
      const hashedName: string = Converter.hashString(cluster.name);

      await this.s3.upload({
        Bucket: AWSService.getBucketConfig(cluster).usedBucked,
        Key: `${hashedName}/`,
        Body: ''
      }).promise();

      return hashedName;
    } catch (error) {
      throw new HttpError(error.status, error.message);
    }
  }

  /**
   * @private
   * @static
   * @param {IClusterModel} cluster
   * @returns {location: string, usedBucked: string }
   * @memberof AWSService
   */
  private static getBucketConfig(cluster: IClusterModel): { location: string, usedBucked: string } {
    const bucket: { location: string, usedBucked: string } = {
      location: '',
      usedBucked: ''
    };

    switch (cluster.location) {
      case 'fr':
        bucket.location = 'eu-central-1';
        bucket.usedBucked = envConfig.AWS.FRANKFURT_BUCKET;
        break;

      case 'or':
        bucket.location = 'us-west-2';
        bucket.usedBucked = envConfig.AWS.OREGON_BUCKET;
        break;

      case 'sd':
        bucket.location = 'ap-southeast-2';
        bucket.usedBucked = envConfig.AWS.SYDNEY_BUCKET;
        break;
      default: break;
    }

    return bucket;
  }
}

export default new AWSService();
