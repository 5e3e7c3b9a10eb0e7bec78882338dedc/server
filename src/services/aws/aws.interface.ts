import { UploadedFile } from 'express-fileupload';
import { ManagedUpload } from 'aws-sdk/lib/s3/managed_upload';
import { Stream } from 'aws-sdk/clients/glacier';
import { IContentModel } from '../../entities/content/content.model';
import { IClusterModel } from '../../entities/cluster/cluster.model';

/**
 * @export
 * @type {AWSObject} IClusterModel | IContentModel
 */
export type AWSObject = IContentModel | IClusterModel;

/**
 * @export
 * @interface IAWSService
 */
export interface IAWSService {

  /**
   * @async
   * @param {UploadedFile} file
   * @param {IClusterModel} cluster
   * @returns {Promise<ManagedUpload.SendData| any>}
   * @memberof IAWSService
   */
  uploadFile(file: UploadedFile, cluster: IClusterModel): Promise<ManagedUpload.SendData| any>;

  /**
   * @async
   * @param {IContentModel[]} contents
   * @param {IClusterModel} cluster
   * @returns {Promise<any>}
   * @memberof IAWSService
   */
  deleteObjects(cluster: IClusterModel, contents?: IContentModel[]): Promise<any>;

  /**
   * @async
   * @param {string} id
   * @returns {Promise<any>}
   * @memberof IAWSService
   */
  downloadFile(id: string): Promise<any>;

  /**
   * @async
   * @param {IClusterModel} cluster
   * @returns {Promise<string>}
   * @memberof IAWSService
   */
  createCluster(cluster: IClusterModel): Promise<string>;
}
