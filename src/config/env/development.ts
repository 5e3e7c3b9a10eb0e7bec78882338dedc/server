/**
 * development config
 * will replace database config if NODE_ENV === 'development'
 */
import * as dotenv from 'dotenv';
dotenv.load();

export const envConfig: any = {
  database: {
    MONGODB_URI: `mongodb://${process.env.DB_LOGIN}:${process.env.DB_PASS}@${process.env.DB_MBLAB_LINK}`,
    MONGODB_DB_MAIN: process.env.DB_NAME,
  },
  app: {
    client: process.env.FRONTENT_DEVELOP
  },
  AWS: {
    ACCESS_KEY: process.env.AWS_ACCESS_KEY,
    SECRET_KEY: process.env.AWS_SECRET_KEY,
    FRANKFURT_BUCKET: process.env.AWS_BUCKET_NAME_FRANKFURT,
    OREGON_BUCKET: process.env.AWS_BUCKET_NAME_OREGON,
    SYDNEY_BUCKET: process.env.AWS_BUCKET_NAME_SYDNEY,
  },
  EMAIL: {
    SERVICE: process.env.EMAIL_SERVICE,
    LOGIN: process.env.EMAIL_LOGIN,
    PASS: process.env.EMAIL_PASS
  }
};
