/**
 * @interface IError
 */
interface IError {
  status: number;
  message: string;
}

export const errors: any = {
  authFailure: { status: 401, message: 'Authentication failure' },
  inactiveAccount: { status: 401, message: 'Account not activated' },
  forbidden: { status: 403, message: 'Forbidden' },
  notFound: { status: 404, message: 'Not found' },
  serverError: { status: 500, message: 'Something went wrong' },

  getCustomError: (status: number, message: string): IError => {
    return { status, message };
  }
};
