import * as express from 'express';
import UserRouter from '../../entities/user/user.router';
import { IServer } from '../server/ServerInterface';
import ContentRouter from '../../entities/content/content.router';
import ClusterRouter from '../../entities/cluster/cluster.router';

/**
 * @export
 * @class Routes
 */
export default class Routes {
  /**
   * @static
   * @param {IServer} server
   * @memberof Routes
   */
  static init(server: IServer): void {
    const router: express.Router = express.Router();

    // routes
    server.app.use('/v1/user', UserRouter.router);
    server.app.use('/v1/content', ContentRouter.router);
    server.app.use('/v1/cluster', ClusterRouter.router);

    server.app.use(router);
  }
}
