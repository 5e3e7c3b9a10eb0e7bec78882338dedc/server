import * as passport from 'passport';
import * as BearerStrategy from 'passport-http-bearer';
import { IUserService } from '../../entities/user/user.interface';
import UserService from '../../entities/user/user.service';
// tslint:disable-next-line:typedef

/**
 * @export
 * @class Passport
 */
class Passport {
/**
   * @private
   * @type {IUserModelService}
   * @memberof Passport
   */
  private service: IUserService;

  /**
   * Creates an instance of Passport.
   * @param {IUserModelService} repository
   * @memberof UserController
   */
  constructor(service: IUserService) {
    this.service = service;
  }

  /**
   * @static
   * @returns {any}
   * @memberof Passport
   */
  initOAuth2(): void {
    passport.use(
      new BearerStrategy.Strategy((token: string, done: any): any => {
        this.service.checkAccess(token).then((allow: boolean) => {
          if (!allow) return done(null, false);
          
          return done(null, allow, { scope: 'read' });
        });
      }));
  }
}

export default new Passport(UserService);
