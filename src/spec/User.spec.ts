import 'mocha';
import * as chai from 'chai';
import { getUser } from './entities/user';
import { server } from '../config/server/index';
import UserModel, { IUserModel } from '../entities/user/user.model';
const chaiHttp: any = require('chai-http');

chai.use(chaiHttp);

describe('Entity: USER', () => {

  describe('/POST login', () => {
    before((done) => {
      UserModel.remove({ username: 'test_user' }).exec(() => {
        UserModel.create(getUser()).then(() => done());
      });
    });

    it('User failure login. Should return 403 error. Account not activated', () => {
      chai.request(server)
        .post('/v1/user/login')
        .send(getUser().credentials)
        .end((error, result) => {
          chai.expect(result.status).equal(403);
          chai.expect(result.body).to.be.an('object');

          chai.expect(result.body).to.have.property('success');
          chai.expect(result.body.success).equal(false);

          chai.expect(result.body).to.have.property('message');
        });
    });

    it('User failure login. Should return 400 error', () => {
      const user: IUserModel = getUser();
      delete user.credentials.password;
      user.credentials.email = 'tttttttttttt';
      chai.request(server)
      .post('/v1/user/login')
      .send(user.credentials)
      .end((error, result) => {
        chai.expect(result.status).equal(400);
        chai.expect(result.body).to.be.an('object');

        chai.expect(result.body).to.have.property('success');
        chai.expect(result.body.success).equal(false);

        chai.expect(result.body).to.have.property('status');
        chai.expect(result.body.status).equal(400);

        chai.expect(result.body).to.have.property('message');
      });
    });

    it('User successfully login', () => {
      chai.request(server)
        .post('/v1/user/login')
        .send(getUser().credentials)
        .end((error, result) => {
          console.log(result);
          chai.expect(result.status).equal(200);
          chai.expect(result.body).to.be.an('object');
          chai.expect(result.body).to.have.property('success');
          chai.expect(result.body.success).equal(true);
        });
    });
  });
});
